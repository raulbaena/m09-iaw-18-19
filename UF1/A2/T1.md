Crear plantilles
================

MP9UF1A2T1

El sistema de plantilles Genshi

Genshi
------

El paquet de Fedora `python-genshi` instal·la el sistema de plantilles
Genshi i la seva documentació.

Genshi és una biblioteca de Python que proporciona un conjunt integrat
de components per a l’anàlisi, la generació i processament d’HTML, XML o
qualsevol altre contingut textual per a la generació de documents per la
Web. La principal característica és un llenguatge de plantilles, que
està fortament inspirat en Kid i TAL.

Una plantilla de Genshi és un document XML ben format, amb Python
embegut utilitzat per el control de flux i la substitució de variables.
Les plantilles es poden fer servir per generar qualsevol tipus de
sortida en format HTML o XML.

A continuació es mostra una [plantilla](./aux/t01.xml) bàsica de Genshi:

     1 <?python
     2    title =  "A Genshi Template"
     3    fruits = ["apple", "orange", "kiwi"]
     4  ?>
     5  <html xmlns:py="http://genshi.edgewall.org/">
     6    <head>
     7      <title py:content="title">This is replaced.</title>
     8    </head>
     9    <body>
    10      <p>These are some of my favorite fruits:</p>
    11      <ul>
    12        <li py:for="fruit in fruits">
    13          I like ${fruit}s
    14        </li>
    15      </ul>
    16    </body>
    17  </html>

En aquest exemple hi trobem:

-   Un bloc de codi en Python, utilitzant una instrucció
    de processament (habitualment situarem aquest codi en
fitxers separats)
-   La declaració d’espai de noms per Genshi.
-   L’ús de les directives de plantilles (`py:content` i `py:for`).
-   Una expressió de Python: (\${fruit}).

Directives de processament
--------------------------

Les directives de processament de Genshi que primer hem d’estudiar es
poden clasificar tal com mostra aquesta llista:

-   Execució condicional: `py:if`, `py:choose`
-   Bucles: `py:for`
-   Definició de variables: `py:with`
-   Generació d’XML: `py:attrs`, `py:content`, `py:replace` i `py:strip`

Expansió de plantilles
----------------------

Per expandir les plantilles usarem de moment l’script
[render.py](aux/render.py), i expandirem les plantilles en el terminal,
sense involucrar en aquesta tasca a servidor web o scripts CGI. Aquestes
ordres mostren com expandir diferents plantilles de prova situades en el
directori [aux](aux):

-   `$ python ./render.py t01.xml`
-   `$ title="valor heretat en entorn" python ./render.py t02.xml`
-   `$ name=Joan times=10 python ./render.py t03.xml | xmllint --format -`

Pot ser instructiu estudiar l’script [render.py](aux/render.py), ja que
mes endavant et caldrà fer servir un codi similar en els scripts CGI:

     2  """Expand genshi templates.
     3  Usage: env N=V... python ./render.py TEMPLATE
     4  """

     5  import os
     6  import sys
     7  from genshi.template import TemplateLoader

     8  TEMPLATE = sys.argv[1]

     9  TPATH = ["./", "/opt/templates"] # directoris on cercar plantilles

    10  loader = TemplateLoader(TPATH)

    11  template = loader.load(TEMPLATE)

    12  stream = template.generate(**os.environ)

    13  sys.stdout.write(stream.render(encoding="UTF-8"))

Llegeix la documentació de la línia 3: ho entens tot?

Tècniques especials de Python
-----------------------------

Si no coneixies encara la tècnica emprada en la línia 12 estudia aquests
exemples:

    >>> t = (5, 6)
    >>> def f(a=None, b=None):
    ...  print a, b
    ... 
    >>> f()
    None None
    >>> f(7, 8)
    7 8
    >>> f(*t)
    5 6
    >>> f(b=9)
    None 9
    >>> d = dict(a=1, b=2)
    >>> f(**d)
    1 2
    >>> d = { "a": 11, "b": 22 }
    >>> f(**d)
    11 22

Enllaços recomanats
-------------------

-   En la documentació local de Genshi: [Genshi XML Template
    Language](file:///usr/share/doc/python-genshi/doc/xml-templates.html).

Pràctiques
----------

De moment treballarem les plantilles sense usar servidor Web o scripts
CGI. Simplement expandirem les plantilles en la sortida estàndard i
estudiarem el resultat.

1.  Consulta la documentació instal·lada amb el paquet `python-genshi`
    que es recomana en els enllaços recomanats.
2.  Estudia i expandeix (amb [render.py](aux/render.py)) les plantilles
    de prova situades en el directori [aux](aux) (fitxers amb extensió
    `xml`).
3.  Fes una plantilla que mostri els arguments rebuts en executar a
    [render.py](aux/render.py) (llista `sys.argv`).
4.  Fes plantilles de prova per els exemples presentats en la
    documentació local de
    [Genshi](file:///usr/share/doc/python-genshi/doc/index.html):
    *Genshi XML Template Language*.
5.  Fes una plantilla que mostri les primeres 16 potencies de 2 (`2**1`,
    `2**2`, `2**3`, etc.).
6.  Fes una plantilla que mostri la taula de multiplicar.

