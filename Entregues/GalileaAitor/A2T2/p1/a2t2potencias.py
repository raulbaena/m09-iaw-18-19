#!/usr/bin/python
# -*- coding: utf-8 -*-
# ejercicio 1 de A2 T2
# calcular las primeras 16 potencias de 2

import os
import sys

from genshi.template import TemplateLoader
def pagina(template, environment):
  # template: nom fitxer XML
  # environment: diccionari de valors a usar per la plantilla

  path = ["./", "/opt/templates"] # directoris on cercar plantilles

  loader = TemplateLoader(path)   #instanciem un objecte TemplateLoader 

  template = loader.load(template) #carreguem plantilla amb mètode load

  stream = template.generate(**environment) # pasem entorn a plantilla

  sys.stdout.write("Content-Type: text/html\r\n\r\n")
  sys.stdout.write(stream.render(encoding="UTF-8"))

plantilla = "a2t2potencias.html"
entorno = {'numpots' : 16}

pagina(plantilla, entorno)

exit(0)

# vim:sw=4:ts=4:ai

