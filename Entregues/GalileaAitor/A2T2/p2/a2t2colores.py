#!/usr/bin/python
# -*- coding: utf-8 -*-

import os, sys, session, time, random

from genshi.template import TemplateLoader
write = sys.stdout.write

########################################################################
# Creació resposta
########################################################################

def pagina(template, environment):
  # template: nom fitxer XML
  # environment: diccionari de valors a usar per la plantilla

  path = ["./", "/opt/templates"] # directoris on cercar plantilles

  loader = TemplateLoader(path)   #instanciem un objecte TemplateLoader 

  template = loader.load(template) #carreguem plantilla amb mètode load

  stream = template.generate(**environment) # pasem entorn a plantilla

  sys.stdout.write("Content-Type: text/html\r\n\r\n")
  sys.stdout.write(stream.render(encoding="UTF-8"))

########################################################################
# Càlcul i definició variables
########################################################################
colores = ("aqua", "black", "blue", "fuchsia", "gray", "green", "lime", "maroon", "navy", "olive", "purple", "red",
           "silver", "teal", "white", "yellow")
randcolor = random.choice(colores)
cookies = session.request_cookies()
session.response_cookie("LAST-VISIT", randcolor)

if cookies is None:
    canterior = "Primera visita o Cookies desactivadas"
else:
    canterior = cookies["LAST-VISIT"]
entorno = {
    'color' : randcolor ,
    'coloranterior' : canterior
}

plantilla = "a2t2colores.html"

########################################################################
# Processament petició
########################################################################

# si cal modificar entorn...
# processar peticio...
# ...

pagina(plantilla, entorno)

sys.exit(0) 

