#!/usr/bin/python
# -*- coding: utf-8 -*-

import os, sys, session, time

from genshi.template import TemplateLoader
write = sys.stdout.write

########################################################################
# Creació resposta
########################################################################

def pagina(template, environment):
  # template: nom fitxer XML
  # environment: diccionari de valors a usar per la plantilla

  path = ["./", "/opt/templates"] # directoris on cercar plantilles

  loader = TemplateLoader(path)   #instanciem un objecte TemplateLoader 

  template = loader.load(template) #carreguem plantilla amb mètode load

  stream = template.generate(**environment) # pasem entorn a plantilla

  sys.stdout.write("Content-Type: text/html\r\n\r\n")
  sys.stdout.write(stream.render(encoding="UTF-8"))

########################################################################
# Càlcul i definició variables
########################################################################

cookies = session.request_cookies()

if cookies is None:
    #color de fondo actual
    canterior = "transparent"
    #color de fondo de la siguiente pagina
    newcolor = "yellow"
elif cookies["LAST-COLOR"] == "yellow":
    newcolor = "green"
    canterior = "yellow"
else:
    newcolor = "yellow"
    canterior = "green"

write(session.response_cookie("LAST-COLOR", newcolor))

entorno = {
    'color' : canterior ,
    'colorsgte' : newcolor
}

plantilla = "a2t2coloralterno.html"

########################################################################
# Processament petició
########################################################################

# si cal modificar entorn...
# processar peticio...
# ...

pagina(plantilla, entorno)

sys.exit(0) 

