#Entrega practicas HTML dinámico con plantillas
##Contenido
* A2T1: ejercicios 5 y 6 de plantillas, archivo de renderizado. Para comprobar el funcionamiento de la plantilla hay que ejecutar el comando
    ```
    python render.py ej5.html
    python render.py ej6.html 
    ```
* A2T2 contiene dos subdirectorios:
  * p1: ejercicios 1 y 2 de la primera parte de A2T2, son los mismos de A2T1 pero esta vez con script + plantilla. 
  * p2: ejercicios 1, 2 y 3 de la segunda parte de A2T2  
  
    Se comprueban copiándolos en un directorio de publicación web y accediendo con un navegador al archivo .py. Todos 
    necesitan el archivo session.py en el mismo directorio y que la plantilla html se encuentre en el mismo directorio 
    que el archivo .py o en el directorio /opt/templates.
    
##Observaciones
En el ejercicio 1 de A2T2/p2 (cambio de color aleatorio), el programa genera un color aleatorio para la sesión actual y 
recupera el color de la sesión anterior de la cookie. Se ha hecho así intencionadamente, ya que en el texto del ejercicio
no se especifica y me pareció más lógico recuperar un dato de una sesión anterior que generar un dato para una hipotética
sesión futura. En cualquier caso en el siguiente ejercicio se usa la mecánica de generar el color que se va a usar en la 
siguiente sesión.

En todos los ejercicios de cookies es importante limpiar las cookies antes de cambiar de ejercicio, o se pierde un montón
de tiempo buscando errores que no existen
