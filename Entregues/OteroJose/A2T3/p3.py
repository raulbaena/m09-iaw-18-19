#!/usr/bin/python
# -*- coding: utf-8 -*-

import os, sys
import session

from genshi.template import TemplateLoader

########################################################################
# Creació resposta
########################################################################

def pagina(template, environment):
  # template: nom fitxer XML
  # environment: diccionari de valors a usar per la plantilla

  path = ["./", "/opt/templates"] # directoris on cercar plantilles

  loader = TemplateLoader(path)

  template = loader.load(template)

  stream = template.generate(**environment) # pasem entorn a plantilla

  sys.stdout.write("Content-Type: text/html\r\n\r\n")
  sys.stdout.write(stream.render(encoding="UTF-8"))

########################################################################
# Càlcul i definició variables
########################################################################

## hacemos el request cooki

#obtenemos la info de la cooki usando el modulo session
Info_cooki = session.request_cookies()

# miramos si existe informacion en la cooki

#si no hay info en la cookie el token sera True
if Info_cooki is None:
    publicidad = True
else:
    publicidad = False


#### formateamos la salida del xml
if publicidad == True:
  valor ="activada"
else:
  valor ="desactivada"

# guardamos la informacion en la cooki
sys.stdout.write(session.response_cookie("publicidad", False))

entorn = {
  'token': publicidad,
  'valor': valor,
  'color': 'green'

}

plantilla = "p3.xml"

########################################################################
# Processament petició
########################################################################

# si cal modificar entorn...
# processar peticio...
# ...

pagina(plantilla, entorn)

sys.exit(0)



# vim:sw=4:ts=4:ai
