#!/usr/bin/python
# -*- coding: utf-8 -*-

import os, sys
import session

from genshi.template import TemplateLoader

########################################################################
# Creació resposta
########################################################################

def pagina(template, environment):
  # template: nom fitxer XML
  # environment: diccionari de valors a usar per la plantilla

  path = ["./", "/opt/templates"] # directoris on cercar plantilles

  loader = TemplateLoader(path)

  template = loader.load(template)

  stream = template.generate(**environment) # pasem entorn a plantilla

  sys.stdout.write("Content-Type: text/html\r\n\r\n")
  sys.stdout.write(stream.render(encoding="UTF-8"))

########################################################################
# Càlcul i definició variables
########################################################################

## hacemos el request cooki

#obtenemos la info de la cooki usando el modulo session
Info_cooki = session.request_cookies()

# miramos si existe informacion anterior, si no es asi le asignamos un color
#transparente, si existe alternamos colores

if Info_cooki is None:
    color_actual = "transparent"
    proximo_color= "yellow"

#buscamos en el diccionario de la cooki

if Info_cooki["COLOR_COOKIE"] == "yellow":
    proximo_color = "green"
    color_actual = "yellow"
else:
    proximo_color = "yellow"
    color_actual = "green"

# guardamos la informacion en la cooki
sys.stdout.write(session.response_cookie("COLOR_COOKIE", proximo_color))

entorn = {
  'color': color_actual,
  'next_color':proximo_color
}

plantilla = "p2.xml"

########################################################################
# Processament petició
########################################################################

# si cal modificar entorn...
# processar peticio...
# ...

pagina(plantilla, entorn)

sys.exit(0)



# vim:sw=4:ts=4:ai
