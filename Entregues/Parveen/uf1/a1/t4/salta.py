#!/usr/bin/python
#-*- coding: utf-8-*-


'''Test CGI script.
'''

import sys
import cgi
import random



write = sys.stdout.write
form = cgi.FieldStorage()



URL= random.choice(['www.hola.com','www.facebook.com','www.escoladeltreball.org'])

# headers
write('Content-Type: text/plain; charset=UTF-8\r\n')
write('Location: http://%s \r\n' %(URL))
write('\r\n')


sys.exit(0)
