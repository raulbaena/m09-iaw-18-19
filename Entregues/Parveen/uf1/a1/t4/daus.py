#!/usr/bin/python
#-*- coding: utf-8-*-



'''Test CGI script.
'''

import sys
import cgi
import random

# Debug
#import cgitb; cgitb.enable()

write = sys.stdout.write
form = cgi.FieldStorage()



result1 = random.randrange(6)
result2 = random.randrange(6)

# headers
write("Content-Type: text/html; charset=UTF-8\r\n") # or "text/html"...
write("\r\n")


template = "<html><body><p>El nuumero de la tirada de dados es:  {respuesta1} y {respuesta2}!!!!</p>"
print(template.format(respuesta1=result1, respuesta2=result2))
sys.exit(0)
