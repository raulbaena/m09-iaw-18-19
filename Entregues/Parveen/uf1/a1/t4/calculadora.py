#!/usr/bin/python
#-*- coding: utf-8-*-
# python /usr/lib64/python2.*/CGIHTTPServer.py
# REQUEST_METHOD=GET QUERY_STRING='N=4&M=4&OP=div' ./calcula.py


'''Test CGI script.
'''
# libreria
import sys
import cgi
import os, sys
import urllib

# Capturamos la QS
write = sys.stdout.write

camps = os.environ['QUERY_STRING'].split("&")

for i in range(len(camps)):
    camps[i] = camps[i].split("=", 1)

for i in range(len(camps)):
    camps[i] = (camps[i][0], urllib.unquote_plus(camps[i][1]))

for (k, v) in camps:
    write("%s = %s\n" % (k, v))


answer = ""
operador = camps[2][1]
m = camps[0][1]
n = camps[1][1]

# operamos

try:
	m = float(m)
	n = float(n)

except ValueError:
	m = 0
	n = 0

# Comprovem operador
if operador == 'sum':
	res = m + n
	operador = '+'

elif operador == 'res':
	res = m - n
	operador = '&minus;'

elif operador == 'mul':
	res = m * n
	operador = '&times;'

elif operador == 'div':
	if n != 0:
		res = m / n
		operador = '&divide;'
	else:
		res = -1
		operador = '&divide;'
else:
	res = m + n
	operador = '+'


template = "La answer es {answer}"
print(template.format(answer=res))
sys.exit(0)
