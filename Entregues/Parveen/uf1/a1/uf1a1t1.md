# Practica01

### Must need to be install as root 

```
dnf -y install apachetop httpd-tools httpd httpd-manual
```
**write servername in httpd file localhost**

### Start server with when cumpet is start and start now

```
systemctl start httpd
systemctl enable httpd
```
### Need to add group and user for this practica

```
groupadd webmaster
useradd -g webmaster webmaster
```

### Make this file and change permison 

```
mkdir /var/www/html
mkdir /var/www/cgi-bin
chown webmaster.webmaster /var/www/html/
chown webmaster.webmaster /var/www/cgi-bin/
```

### Copy file dump.sh form practica part put  in cgi-bin change as 
executable

```
cp /tmp/dump.sh /var/www/cgi-bin
chmod +x /var/www/cgi-bin/dump.sh
systemctl restart httpd
```
### Make file in formato python and give permmisso to executable

**Try on**
```
 http://localhost/cgi-bin/dump.py
```

**Also work this page very well**

```
http://localhost/manual

```

![ screen_shot_1](./scr1.png)

![ screen_shot_2](./scr2.png)



