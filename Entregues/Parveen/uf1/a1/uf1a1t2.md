# Practica02

**check server httpd working**

cheack in browser

**http://localhost** 

```
[isx0276204@i01 ~]$ telnet localhost 80
Trying ::1...
Connected to localhost.
Escape character is '^]'.
GET /index.html
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ca" lang="ca">
  <head><title>Salutacions</title></head>
  <body>
    <h1>Salutacions</h1>
    <p>Uep!</p>
  </body>
</html>

Connection closed by foreign host.
```

**check difference between 1.0 and 1.1**

```
[root@i01 ~]# telnet localhost 80
Trying ::1...
Connected to localhost.
Escape character is '^]'.
GET / HTTP/1.0

HTTP/1.1 200 OK
Date: Mon, 11 Mar 2019 10:31:32 GMT
Server: Apache/2.4.34 (Fedora) OpenSSL/1.1.0h-fips mod_perl/2.0.10 Perl/v5.26.2
Last-Modified: Tue, 05 Mar 2019 08:02:59 GMT
ETag: "121-583544d2f9074"
Accept-Ranges: bytes
Content-Length: 289
Connection: close
Content-Type: text/html; charset=UTF-8

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ca" lang="ca">
  <head><title>Salutacions</title></head>
  <body>
    <h1>Salutacions</h1>
    <p>Uep!</p>
  </body>
</html>

Connection closed by foreign host.

```


```
[root@i01 ~]# telnet localhost 80
Trying ::1...
Connected to localhost.
Escape character is '^]'.
GET / HTTP/1.1
host: localhost

HTTP/1.1 200 OK
Date: Mon, 11 Mar 2019 10:47:40 GMT
Server: Apache/2.4.34 (Fedora) OpenSSL/1.1.0h-fips mod_perl/2.0.10 Perl/v5.26.2
Last-Modified: Tue, 05 Mar 2019 08:02:59 GMT
ETag: "121-583544d2f9074"
Accept-Ranges: bytes
Content-Length: 289
Content-Type: text/html; charset=UTF-8

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ca" lang="ca">
  <head><title>Salutacions</title></head>
  <body>
    <h1>Salutacions</h1>
    <p>Uep!</p>
  </body>
</html>

Connection closed by foreign host.

```

