#!/usr/bin/python
# -*- coding: utf-8 -*

"""Test CGI script with one cookie.
"""

import os,sys,time
from genshi.template import TemplateLoader
write = sys.stdout.write



########################################################################
# exported functions
########################################################################
_DATE_FORMAT = "%a, %d-%b-%Y %H:%M:%S GMT"

def request_cookies():
    """Return HTTP request cookies as a dictionary."""

    s = os.environ.get("HTTP_COOKIE") # header string with 1 or more cookies
    return None if s is None else dict(c.split("=", 1) for c in s.split("; "))

def response_cookie(key, value, expires=None):
    """Return HTTP response header for a cookie."""

    if expires is None:     # session cookie (expires closing the browser)
        return "Set-Cookie: %s=%s\r\n" % (key, value)
    elif expires <= 0:      # clear cookie (explicit expiration)
        t = time.strftime(_DATE_FORMAT, time.gmtime(0))
        return "Set-Cookie: %s=%s; expires=%s\r\n" % (key, value, t)
    else:                   # set expiration time in the future
        t = time.strftime(_DATE_FORMAT, time.gmtime(time.time() + expires))
        return "Set-Cookie: %s=%s; expires=%s\r\n" % (key, value, t)

############################################################################

# Creacio resposta
def pagina(template, environment):
  # template: nom fitxer XML
  # environment: diccionari de valors a usar per la plantilla

  path = ["./", "/opt/templates"] # directoris on cercar plantilles

  loader = TemplateLoader(path)

  template = loader.load(template)

  stream = template.generate(**environment) # pasem entorn a plantilla

  write("Content-Type: text/html\r\n")
  
  write("\r\n")
  
  #write("<p>okkkkkkkkkkkkk</p>")
  
  write(stream.render(encoding="UTF-8"))

########################################################################





# received cookies leer cookies
cookies = request_cookies()

#print cookies

# response body
#write("<html>\n<head><title>color change</title></head>\n<body>\n")
#write("<p>next_backgroung_color : %s</p>" % (select_color))

if cookies is None :
	check = True
	check_again = False
	
else:
	check = False
	check_again = False
	
		
sys.stdout.write(response_cookie("check_again",check_again))		


##############################################################################


########################################################################
# Calcul i definicio variables
########################################################################

entorn = {
        'check' : check
        }

plantilla = "ex03.xml"

########################################################################
# Processament peticio
########################################################################

# si cal modificar entorn...
# processar peticio...
# ...

pagina(plantilla, entorn)

sys.exit(0)

# vim:sw=4:ts=4:ai:et
