#!/usr/bin/python

"""Test CGI script with one cookie.
"""

import os
import sys
import time,cgi

import random

from genshi.template import TemplateLoader

_DATE_FORMAT = "%a, %d-%b-%Y %H:%M:%S GMT"

########################################################################
# exported functions
########################################################################

def request_cookies():
    """Return HTTP request cookies as a dictionary."""

    s = os.environ.get("HTTP_COOKIE") # header string with 1 or more cookies
    return None if s is None else dict(c.split("=", 1) for c in s.split("; "))

def response_cookie(key, value, expires=None):
    """Return HTTP response header for a cookie."""

    if expires is None:     # session cookie (expires closing the browser)
        return "Set-Cookie: %s=%s\r\n" % (key, value)
    elif expires <= 0:      # clear cookie (explicit expiration)
        t = time.strftime(_DATE_FORMAT, time.gmtime(0))
        return "Set-Cookie: %s=%s; expires=%s\r\n" % (key, value, t)
    else:                   # set expiration time in the future
        t = time.strftime(_DATE_FORMAT, time.gmtime(time.time() + expires))
        return "Set-Cookie: %s=%s; expires=%s\r\n" % (key, value, t)

############################################################################

write = sys.stdout.write

list_color = ['aqua', 'black', 'blue', 'fuchsia',
'gray','green', 'lime', 'maroon', 'navy', 'olive', 'purple', 'red',
'silver', 'teal', 'white','yellow']
select_color = random.choice(list_color)

# HTTP headers
# save cookie
write(response_cookie("color",select_color))
write("Content-Type: text/html\r\n")
write("\r\n")

# received cookies leer cookies
cookies = request_cookies()

# response body
write("<html>\n<head><title>color random</title></head>\n<body>\n")
write("<p>next_backgroung_color : %s</p>" % (select_color))
if cookies is None:
		color = 'white'
else:
	color = cookies['color']
##############################################################################
#############################################################################
# Creacio resposta

def pagina(template, environment):
  # template: nom fitxer XML
  # environment: diccionari de valors a usar per la plantilla

  path = ["./", "/opt/templates"] # directoris on cercar plantilles

  loader = TemplateLoader(path)

  template = loader.load(template)

  stream = template.generate(**environment) # pasem entorn a plantilla

  sys.stdout.write(stream.render(encoding="UTF-8"))

########################################################################
# Calcul i definicio variables
########################################################################

entorn = {
        'color' : color
        }

plantilla = "ex01.xml"

########################################################################
# Processament peticio
########################################################################

# si cal modificar entorn...
# processar peticio...
# ...

pagina(plantilla, entorn)

sys.exit(0)




# vim:sw=4:ts=4:ai:et
