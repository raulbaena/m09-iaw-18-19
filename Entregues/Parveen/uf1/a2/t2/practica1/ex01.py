#!/usr/bin/python

import os, sys,cgi

from genshi.template import TemplateLoader

########################################################################
# Creacio resposta
########################################################################

def pagina(template, environment):
  # template: nom fitxer XML
  # environment: diccionari de valors a usar per la plantilla

  path = ["./", "/opt/templates"] # directoris on cercar plantilles

  loader = TemplateLoader(path)

  template = loader.load(template)

  stream = template.generate(**environment) # pasem entorn a plantilla

  sys.stdout.write("Content-Type: text/html\r\n\r\n")
  sys.stdout.write(stream.render(encoding="UTF-8"))

########################################################################
# Calcul i definicio variables
########################################################################

entorn = {
        'number' : 16
        }

plantilla = "ex01.xml"

########################################################################
# Processament peticio
########################################################################

# si cal modificar entorn...
# processar peticio...
# ...

pagina(plantilla, entorn)

sys.exit(0) 

