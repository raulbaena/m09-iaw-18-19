# PARCTICA uf1_a2_t1

### ex02

```
[root@i01 uf1_a2_t1]# python ./render.py t01.xml 
<html lang="en">
  <head>
    <title>A Genshi Template</title>
  </head>
  <body>
    <p>These are some of my favorite fruits:</p>
    <ul>
      <li>
        I like apples
      </li><li>
        I like oranges
      </li><li>
        I like kiwis
      </li>
    </ul>
  </body>
```

### ex03

```
[root@i01 uf1_a2_t1]# python ./render.py ex3.xml hola caracola
<html lang="en">
  <head>
    <title>A Genshi Template</title>
  </head>
  <body>
    <p>These are some of my favorite fruits:</p>
    <ul>
      <li>
        I like ex3.xml
      </li><li>
        I like hola
      </li><li>
        I like caracola
      </li>
    </ul>
  </body>

```
